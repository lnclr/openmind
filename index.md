---
title: "reco"
order: 0
in_menu: true
---
Liste fabriquée avec [https://beta.framalibre.org/mini-site](https://beta.framalibre.org/mini-site)

# Logiciels libres 
## Mail


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Thunderbird.png">
    </div>
    <div>
      <h2>Thunderbird</h2>
      <p>Célèbre client de courriel issu du projet Mozilla, distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/thunderbird.html">Vers la notice Framalibre</a>
        <a href="https://www.thunderbird.net/fr/">Vers le site</a>
      </div>
    </div>
  </article>

Alternative Android :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FairEmail.png">
    </div>
    <div>
      <h2>FairEmail</h2>
      <p>Client Android de messagerie, minimaliste privilégiant la lecture et la rédaction, orienté sécurité.</p>
      <div>
        <a href="https://framalibre.org/notices/fairemail.html">Vers la notice Framalibre</a>
        <a href="https://email.faircode.eu/">Vers le site</a>
      </div>
    </div>
  </article>



## Prise de notes synchronisées PC / Android / Nextcloud

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Joplin.svg">
    </div>
    <div>
      <h2>Joplin</h2>
      <p>Joplin est une application gratuite et open source de prise de notes et de tâches qui peut gérer un grand nombre de notes organisées en blocs.</p>
      <div>
        <a href="https://framalibre.org/notices/joplin.html">Vers la notice Framalibre</a>
        <a href="https://joplinapp.org/">Vers le site</a>
      </div>
    </div>
  </article>


## Pour écouter des podcasts
Sur PC 
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/GPodder.jpg">
    </div>
    <div>
      <h2>GPodder</h2>
      <p>Ce logiciel vous permettra de suivre vos podcast ainsi que vos chaines youtube favorites.</p>
      <div>
        <a href="https://framalibre.org/notices/gpodder.html">Vers la notice Framalibre</a>
        <a href="http://gpodder.org/">Vers le site</a>
      </div>
    </div>
  </article>

Sur Android
  <article class="framalibre-notice">
    <div>
      <img src="https://beta.framalibre.org/images/logo/AntennaPod.png">
    </div>
    <div>
      <h2>AntennaPod</h2>
      <p>Un gestionnaire de Podcast pour Android.</p>
      <div>
        <a href="https://beta.framalibre.org/notices/antennapod.html">Vers la notice Framalibre</a>
        <a href="http://antennapod.org/">Vers le site</a>
      </div>
    </div>
  </article> 

>  https://gpodder.net/ ne semble plus fonctionner. Une recommandation pour un système de synchronisation  multi-plateformes de podcasts est la bienvenue !


## Bibliothèque d'images

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Shotwell.png">
    </div>
    <div>
      <h2>Shotwell</h2>
      <p>Un logiciel de gestion de photothèque avancé, avec des options de retouche.</p>
      <div>
        <a href="https://framalibre.org/notices/shotwell.html">Vers la notice Framalibre</a>
        <a href="https://wiki.gnome.org/Apps/Shotwell">Vers le site</a>
      </div>
    </div>
  </article>


## Retouche d'images

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/GIMP.png">
    </div>
    <div>
      <h2>GIMP</h2>
      <p>Un puissant logiciel d'édition et de retouche d'images.</p>
      <div>
        <a href="https://framalibre.org/notices/gimp.html">Vers la notice Framalibre</a>
        <a href="https://www.gimp.org/">Vers le site</a>
      </div>
    </div>
  </article>

## Création et modification d'images (vectoriel)

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Inkscape.png">
    </div>
    <div>
      <h2>Inkscape</h2>
      <p>Un puissant logiciel de dessin vectoriel.</p>
      <div>
        <a href="https://framalibre.org/notices/inkscape.html">Vers la notice Framalibre</a>
        <a href="https://inkscape.org/fr/">Vers le site</a>
      </div>
    </div>
  </article>


## Navigateur internet
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Firefox.png">
    </div>
    <div>
      <h2>Firefox</h2>
      <p>Le navigateur web épris de liberté distribué par la Fondation Mozilla.</p>
      <div>
        <a href="https://framalibre.org/notices/firefox.html">Vers la notice Framalibre</a>
        <a href="https://www.mozilla.org/fr/firefox/">Vers le site</a>
      </div>
    </div>
  </article>

## Editeur de code
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Geany.png">
    </div>
    <div>
      <h2>Geany</h2>
      <p>Environnement de développement multiplate-formes léger et modulaire.</p>
      <div>
        <a href="https://framalibre.org/notices/geany.html">Vers la notice Framalibre</a>
        <a href="http://www.geany.org/">Vers le site</a>
      </div>
    </div>
  </article>

Alternative :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Bluefish.png">
    </div>
    <div>
      <h2>Bluefish</h2>
      <p>Un éditeur de texte simple et efficace.</p>
      <div>
        <a href="https://framalibre.org/notices/bluefish.html">Vers la notice Framalibre</a>
        <a href="http://bluefish.openoffice.nl/index.html">Vers le site</a>
      </div>
    </div>
  </article>



## Transfert de fichiers FTP
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/FileZilla.png">
    </div>
    <div>
      <h2>FileZilla</h2>
      <p>FileZilla est un client FTP, FTPS et sFTP populaire.</p>
      <div>
        <a href="https://framalibre.org/notices/filezilla.html">Vers la notice Framalibre</a>
        <a href="https://filezilla-project.org/">Vers le site</a>
      </div>
    </div>
  </article>



## Système de gestion de contenu de site internet (CMS)
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Spip.png">
    </div>
    <div>
      <h2>Spip</h2>
      <p>Très puissant, SPIP peut gérer un petit blog comme un gros site, voire une boutique ecommerce, grâce à son système de « squelettes » et ses multiples extensions.</p>
      <div>
        <a href="https://framalibre.org/notices/spip.html">Vers la notice Framalibre</a>
        <a href="http://www.spip.net/">Vers le site</a>
      </div>
    </div>
  </article>

Alternative :

  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/WordPress.png">
    </div>
    <div>
      <h2>WordPress</h2>
      <p>Le CMS le plus utilisé au monde</p>
      <div>
        <a href="https://framalibre.org/notices/wordpress.html">Vers la notice Framalibre</a>
        <a href="https://fr.wordpress.org/">Vers le site</a>
      </div>
    </div>
  </article>


## Stockage et synchronisation de fichiers (webdav), de calendrier (caldav), de contacts (carddav), de flux RSS,...
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Nextcloud.png">
    </div>
    <div>
      <h2>Nextcloud</h2>
      <p>Nextcloud est une plate-forme auto-hébergée de services de stockage et d’applications diverses dans les nuages.</p>
      <div>
        <a href="https://framalibre.org/notices/nextcloud.html">Vers la notice Framalibre</a>
        <a href="https://nextcloud.com/">Vers le site</a>
      </div>
    </div>
  </article>

## Lecture et conversion de fichers ebooks, PDF,..
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Calibre.png">
    </div>
    <div>
      <h2>Calibre</h2>
      <p>Gestionnaire de bibliothèque de livres numériques.</p>
      <div>
        <a href="https://framalibre.org/notices/calibre.html">Vers la notice Framalibre</a>
        <a href="https://calibre-ebook.com/">Vers le site</a>
      </div>
    </div>
  </article>


  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/PDF%20Arranger.png">
    </div>
    <div>
      <h2>PDF Arranger</h2>
      <p>Un logiciel simple pour extraire, assembler, pivoter et réordonner des pages de documents PDF.</p>
      <div>
        <a href="https://framalibre.org/notices/pdf-arranger.html">Vers la notice Framalibre</a>
        <a href="https://github.com/pdfarranger/pdfarranger">Vers le site</a>
      </div>
    </div>
  </article>


## Lecture et conversion de fichers audio et vidéo
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/VLC.png">
    </div>
    <div>
      <h2>VLC</h2>
      <p>VLC est un lecteur multimédia très populaire.</p>
      <div>
        <a href="https://framalibre.org/notices/vlc.html">Vers la notice Framalibre</a>
        <a href="https://www.videolan.org/vlc/">Vers le site</a>
      </div>
    </div>
  </article> 

## Utilitaire URLs
  <article class="framalibre-notice">
    <div>
      <img src="https://framalibre.org/images/logo/Yourls.svg">
    </div>
    <div>
      <h2>Yourls</h2>
      <p>YOURLS est un puissant paquet de scripts PHP pour vous permettre de mettre en œuvre, sur votre serveur, votre propre raccourcisseur de liens..</p>
      <div>
        <a href="https://framalibre.org/notices/yourls.html">Vers la notice Framalibre</a>
        <a href="https://yourls.org/">Vers le site</a>
      </div>
    </div>
  </article> 